﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Tire damagedTire;
    public GameObject[] damagedTireScrewHoles;
    public Tire newTire;

    public int numNutsUnscrewed = 5;
    public int numNutsScrewed = 0;

    public GameObject countdownPanel;
    public Text countdownText;
    public float countdownTime = 3.0f;

    public float time = 20.0f;
    public bool timerOn = false;
    public Text timerText;

    public GameObject gameWinPanel;
    public GameObject gameLosePanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Countdown()
    {
        countdownPanel.SetActive(true);
        while (countdownTime > 0.0f)
        {
            countdownTime -= Time.unscaledDeltaTime;
            countdownText.text = countdownTime.ToString("0");
            yield return null;
        }
        Time.timeScale = 1.0f;
        countdownPanel.SetActive(false);
        StartTimer();

        if (SoundPlayer.instance != null)
        {
            SoundPlayer.instance.PlayMainMusic();
        }
    }

    private void Start()
    {
        if (SoundPlayer.instance != null)
        {
            SoundPlayer.instance.musicSource.Stop();
        }

        gameWinPanel.SetActive(false);
        gameLosePanel.SetActive(false);

        Time.timeScale = 0.0f;
        StartCoroutine(Countdown());
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        if (SoundPlayer.instance != null)
        {
            SoundPlayer.instance.PlayAmbientRacingSounds();
        }
    }

    private void AllowTireSwitch()
    {
        damagedTire.gameObject.layer = LayerMask.NameToLayer("Tire");
        damagedTire.GetComponent<Rigidbody2D>().bodyType =
            RigidbodyType2D.Dynamic;
        for (int i = 0; i < damagedTireScrewHoles.Length; i++)
        {
            damagedTireScrewHoles[i].tag = "Untagged";
        }

        newTire.gameObject.layer = LayerMask.NameToLayer("Tire");
    }

    public void DecreaseNumUnscrewedNuts()
    {
        numNutsUnscrewed--;
        if (numNutsUnscrewed <= 0)
        {
            AllowTireSwitch();
        }
    }

    public void IncreaseNumScrewedNuts()
    {
        numNutsScrewed++;
        if (numNutsScrewed >= 5)
        {
            timerOn = false;
            if (!gameLosePanel.activeSelf)
            {
                gameWinPanel.SetActive(true);
            }
        }
    }

    public void StartTimer()
    {
        if (!timerOn)
        {
            timerOn = true;
        }
    }

    private void UpdateTimerText()
    {
        timerText.text = time.ToString("00.00");
    }

    private void Update()
    {
        if (timerOn)
        {
            time -= Time.deltaTime;
            if (time <= 0.0f)
            {
                time = 0.0f;
                timerOn = false;

                gameLosePanel.SetActive(true);
            }
            UpdateTimerText();
        }

        if ((gameWinPanel.activeSelf || gameLosePanel.activeSelf)
            && Input.GetMouseButton(0) && Input.GetMouseButton(1))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GoToMainMenu();
        }
    }
}
