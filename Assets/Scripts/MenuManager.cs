﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;

    public GameObject mainMenuPanel;
    public GameObject[] instructionPanels;
    public GameObject optionsPanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals("MainMenu"))
        {
            mainMenuPanel.SetActive(true);
        }
    }

    private void DisableAllPanels()
    {
        mainMenuPanel.SetActive(false);
        foreach (GameObject i in instructionPanels)
        {
            i.SetActive(false);
        }
        optionsPanel.SetActive(false);
    }

    private void Start()
    {
        DisableAllPanels();
        mainMenuPanel.SetActive(true);
        SceneManager.sceneLoaded += OnSceneLoaded;

        if (SoundPlayer.instance != null)
        {
            SoundPlayer.instance.SetMusicVolume();
            SoundPlayer.instance.SetSoundFXVolume();
        }

        Time.timeScale = 1.0f;
    }

    public void ChooseMenuOption(string menuOption)
    {
        switch (menuOption)
        {
            case "Main Menu":
                DisableAllPanels();
                mainMenuPanel.SetActive(true);
                SceneManager.LoadScene("MainMenu");
                break;
            case "Start":
                DisableAllPanels();
                SceneManager.LoadScene("Game");
                break;
            case "Instructions":
                DisableAllPanels();
                ChooseInstructionPanel(1);
                break;
            case "Options":
                DisableAllPanels();
                optionsPanel.SetActive(true);
                break;
            case "Quit":
                Application.Quit();
                break;
        }
    }

    public void ChooseInstructionPanel(int page)
    {
        DisableAllPanels();
        if (page > 0 && page <= instructionPanels.Length)
        {
            instructionPanels[page - 1].SetActive(true);
            SceneManager.LoadScene(page);
        }
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
