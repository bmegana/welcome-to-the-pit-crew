﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Nut : MonoBehaviour
{
    public float numRotationsToUnscrew;
    public float numRotationsToScrew;

    private float lastFrameAngle = 0.0f;
    private float totalDegreesRotated = 0.0f;
    private bool screwing;

    public bool inScrewHole;
    private bool nutHeldOverHole = false;
    private bool screwedInNewTire = false;

    private Vector2 placementArea;

    public enum ScrewDirection
    {
        Clockwise,
        CounterClockwise
    }
    public ScrewDirection screwDirection = ScrewDirection.CounterClockwise;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb2d;

    private GameObject screw;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.bodyType = RigidbodyType2D.Kinematic;
    }

    private void Start()
    {
        placementArea = Vector2.zero;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NutPlacementArea") &&
            !inScrewHole && Input.GetMouseButton(0))
        {
            nutHeldOverHole = true;
            placementArea = collision.transform.position;
            screw = collision.gameObject;
        }
    }

    /* When a player happens to hold a nut over two screw holes, this function
     * ensures that the nut doesn't get dropped even when the player holds the
     * nut over a hole after passing over previous holes. */
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NutPlacementArea") &&
            !inScrewHole && Input.GetMouseButton(0))
        {
            nutHeldOverHole = true;
            placementArea = collision.transform.position;
            screw = collision.gameObject;
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NutPlacementArea") &&
            Input.GetMouseButton(0))
        {
            nutHeldOverHole = false;
            placementArea = Vector2.negativeInfinity;
            screw = null;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            screwing = true;
        }
    }

    private void OnMouseDown()
    {
        if (!inScrewHole)
        {
            transform.position = Camera.main.ScreenToWorldPoint(
                Input.mousePosition
            );
            transform.position = new Vector3(
                transform.position.x, transform.position.y, 0.0f
            );
            rb2d.bodyType = RigidbodyType2D.Kinematic;
        }
    }

    private void OnMouseDrag()
    {
        if (!inScrewHole)
        {
            transform.position = Camera.main.ScreenToWorldPoint(
                Input.mousePosition
            );
            transform.position = new Vector3(
                transform.position.x, transform.position.y, 0.0f
            );
        }
    }

    private void OnMouseUp()
    {
        if (nutHeldOverHole && !inScrewHole)
        {
            totalDegreesRotated = 0.0f;
            transform.position = placementArea;
            rb2d.velocity = Vector2.zero;

            inScrewHole = true;
            screwing = false;
            // Prevent other nuts from attaching to screwhole.
            screw.tag = "Untagged";
        }
        else if (!nutHeldOverHole && !inScrewHole)
        {
            rb2d.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    private void ProcessMouseInput()
    {
        if (inScrewHole && screwing && !screwedInNewTire)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(
                Input.mousePosition
            );
            mousePosition = new Vector3(
                mousePosition.x, mousePosition.y, 0.0f
            );
            float angle = Mathf.Atan2(
                transform.position.y - mousePosition.y,
                transform.position.x - mousePosition.x
            ) * Mathf.Rad2Deg - 90.0f;

            float deltaAngle = lastFrameAngle - angle;
            if (screwDirection == ScrewDirection.CounterClockwise)
            {
                if (-200 < deltaAngle && deltaAngle < 0)
                {
                    transform.rotation = Quaternion.AngleAxis(angle, transform.forward);
                    totalDegreesRotated -= deltaAngle;
                }
            }
            else if (screwDirection == ScrewDirection.Clockwise)
            {
                if (200 > deltaAngle && deltaAngle > 0)
                {
                    transform.rotation = Quaternion.AngleAxis(angle, transform.forward);
                    totalDegreesRotated += deltaAngle;
                }
            }
            lastFrameAngle = angle;

            if (SoundPlayer.instance != null)
            {
                SoundPlayer.instance.PlayWrenchSoundFX();
            }

            if (Input.GetMouseButtonUp(1))
            {
                screwing = false;
                if (SoundPlayer.instance != null)
                {
                    SoundPlayer.instance.soundFXSource.Stop();
                }
            }
        }
    }

    private void Update()
    {
        if (Time.timeScale > 0.0f)
        {
            ProcessMouseInput();

            if (screwDirection == ScrewDirection.CounterClockwise &&
                totalDegreesRotated > (360f * numRotationsToUnscrew))
            {
                if (GameManager.instance != null && inScrewHole)
                {
                    GameManager.instance.DecreaseNumUnscrewedNuts();
                }
                if (SoundPlayer.instance != null && inScrewHole)
                {
                    SoundPlayer.instance.soundFXSource.Stop();
                }

                inScrewHole = false;
                placementArea = Vector2.negativeInfinity;
                rb2d.bodyType = RigidbodyType2D.Dynamic;

                totalDegreesRotated = 0.0f;
                screwDirection = ScrewDirection.Clockwise;
            }
            else if (screwDirection == ScrewDirection.Clockwise &&
                totalDegreesRotated > (360f * numRotationsToScrew))
            {
                if (GameManager.instance != null && !screwedInNewTire)
                {
                    GameManager.instance.IncreaseNumScrewedNuts();
                }
                if (SoundPlayer.instance != null && !screwedInNewTire)
                {
                    SoundPlayer.instance.soundFXSource.Stop();
                }

                screwedInNewTire = true;

                if (spriteRenderer.color != Color.black)
                {
                    spriteRenderer.color = Color.black;
                }
            }
        }
    }
}
