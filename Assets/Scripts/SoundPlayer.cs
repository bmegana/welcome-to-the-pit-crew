﻿using UnityEngine;
using UnityEngine.UI;

public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance;

    public AudioCollection collection;

    public AudioSource musicSource;
    public AudioSource soundFXSource;

    public Slider musicVolumeSlider;
    public Slider soundFXVolumeSlider;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        musicSource.loop = true;
        musicSource.PlayOneShot(collection.ambientRacingSounds);
    }

    public void PlayAmbientRacingSounds()
    {
        musicSource.PlayOneShot(collection.ambientRacingSounds);
    }

    public void PlayMainMusic()
    {
        musicSource.PlayOneShot(collection.mainGameMusic);
    }

    public void PlayWrenchSoundFX()
    {
        if (!soundFXSource.isPlaying)
        {
            soundFXSource.PlayOneShot(collection.wrenchSound);
        }
    }

    public void SetMusicVolume()
    {
        musicSource.volume = musicVolumeSlider.value;
    }

    public void SetSoundFXVolume()
    {
        soundFXSource.volume = soundFXVolumeSlider.value;
    }
}
