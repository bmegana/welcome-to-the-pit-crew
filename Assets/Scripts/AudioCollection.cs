﻿using UnityEngine;

[CreateAssetMenu(
    fileName = "AudioCollection",
    menuName = "ScriptableObjects/AudioCollection",
    order = 1)
]
public class AudioCollection : ScriptableObject
{
    public AudioClip mainGameMusic;
    public AudioClip ambientRacingSounds;

    public AudioClip wrenchSound;
}
