﻿using System.Collections;
using UnityEngine;

public class Blink : MonoBehaviour
{
    public SpriteRenderer[] renderers;
    private Color[] origColors;

    public float timeBlinking = 3.0f;

    private IEnumerator CommenceBlinking()
    {
        while (timeBlinking > 0.0f)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                if (renderers[i].color != origColors[i])
                {
                    renderers[i].color = origColors[i];
                }
                else
                {
                    renderers[i].color = Color.white;
                }
            }
            yield return new WaitForSeconds(0.25f);
            timeBlinking -= .25f;
        }
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].color = origColors[i];
        }
    }

    private void Start()
    {
        origColors = new Color[renderers.Length];
        for (int i = 0; i < renderers.Length; i++)
        {
            origColors[i] = renderers[i].color;
        }
        StartCoroutine(CommenceBlinking());
    }
}
