﻿using UnityEngine;

public class Tire : MonoBehaviour
{
    public bool damaged = false;
    public bool screwedIn = true;

    public GameObject[] screwHoles;

    private Vector2 placementArea;

    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    
    private void Start()
    {
        placementArea = Vector3.negativeInfinity;

        if (!damaged)
        {
            for (int i = 0; i < screwHoles.Length; i++)
            {
                screwHoles[i].tag = "Untagged";
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("TirePlacementArea") &&
            !damaged && Input.GetMouseButton(0))
        {
            placementArea = collision.transform.position;
        }
    }

    private void OnMouseDown()
    {
        if (!screwedIn)
        {
            transform.position = Camera.main.ScreenToWorldPoint(
                Input.mousePosition
            );
            transform.position = new Vector3(
                transform.position.x, transform.position.y, 0.0f
            );

            if (damaged)
            {
                rb2d.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    private void OnMouseDrag()
    {
        if (!screwedIn)
        {
            transform.position = Camera.main.ScreenToWorldPoint(
                Input.mousePosition
            );
            transform.position = new Vector3(
                transform.position.x, transform.position.y, 0.0f
            );
        }
    }

    private void OnMouseUp()
    {
        if (damaged && rb2d.bodyType == RigidbodyType2D.Dynamic)
        {
            rb2d.velocity += new Vector2(
                Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")
            );
        }
        else if (placementArea.x != Mathf.NegativeInfinity &&
            placementArea.y != Mathf.NegativeInfinity &&
            !damaged)
        {
            transform.position = placementArea;
            gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");

            for (int i = 0; i < screwHoles.Length; i++)
            {
                screwHoles[i].tag = "NutPlacementArea";
            }
        }
    }
}
